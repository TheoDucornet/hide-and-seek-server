#include "game.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"

// specific includes
#include "maze/custom/basic.hpp"
#include "maze/custom/default.hpp"
#include "maze/custom/empty.hpp"
#include "maze/custom/closed.hpp"
#include "maze/custom/open.hpp"
#include "player/seeker/exp.hpp"
#include "player/seeker/exp2.hpp"
#include "player/seeker/exp3.hpp"
#include "player/seeker/expFinal.hpp"
#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"
#include "player/hider/exp.hpp"

// test game validity
#include "utils/helper.hpp"

// default params
namespace Params {

    std::string hiderClassName = "exp";
    std::string seekerClassName = "exp2";
    std::string mazeClassName = "closed";

    int nGames = 100;
};

int main(int argc, char* argv[]) {

    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    HiderFactory::registerClass<ExpHider>("exp");
    SeekerFactory::registerClass<ExpSeeker>("exp");
    SeekerFactory::registerClass<ExpSeeker2>("exp2");
    SeekerFactory::registerClass<ExpSeeker3>("exp3");
    SeekerFactory::registerClass<ExpSeekerFinal>("expFinal");
    SeekerFactory::registerClass<RandomSeeker>("random");

    MazeFactory::registerClass<BasicMaze>("basic");
    MazeFactory::registerClass<DefaultMaze>("default");
    MazeFactory::registerClass<EmptyMaze>("empty");
    MazeFactory::registerClass<ClosedMaze>("closed");
    MazeFactory::registerClass<OpenMaze>("open");


    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {
            
            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
            
            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: " 
                                        << "{hider: " << Params::hiderClassName
                                        << ", seeker: " << Params::seekerClassName
                                        << ", maze: " << Params::mazeClassName
                                        << "}" << std::endl;

        // before running
        if (!Helper::isValid(game)) {
            std::cout << "Game is not valid: unexpected maze built..." << std::endl;
            exit(0);
        }
        
        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // update the IA
            player->update(state.first, observation, chosenAction);

            //if(player->getRole() == SEEKER)
                //std::cout<<maze->toString()<<std::endl;

        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in " 
                << game.getNumberOfRounds() << " rounds" << std::endl;
            
        //if(winner==HIDER)
            //std::cout<<maze->toString()<<std::endl;

        // update stat
        results[winner] += 1;
        
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins (" 
                << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }
}