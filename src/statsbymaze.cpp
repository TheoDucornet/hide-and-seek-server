#include "game.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"

// specific includes
#include "maze/custom/basic.hpp"
#include "maze/custom/default.hpp"
#include "maze/custom/empty.hpp"
#include "maze/custom/closed.hpp"
#include "maze/custom/open.hpp"


#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"
#include "player/seeker/exp.hpp"
#include "player/hider/exp.hpp"
#include "player/seeker/expFinal.hpp"

// test game validity
#include "utils/helper.hpp"

// default params
namespace Params {
    std::string hiderClassName = "exp";
    std::string seekerClassName = "expfinal";
    std::string mazeClassName = "empty";
    int nGames = 100;
};

int main(int argc, char* argv[]) {

    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    HiderFactory::registerClass<ExpHider>("exp");
    SeekerFactory::registerClass<ExpSeeker>("exp");
    SeekerFactory::registerClass<ExpSeekerFinal>("expfinal");

    MazeFactory::registerClass<BasicMaze>("basic");
    MazeFactory::registerClass<DefaultMaze>("default");
    MazeFactory::registerClass<EmptyMaze>("empty");
    MazeFactory::registerClass<ClosedMaze>("closed");
    MazeFactory::registerClass<OpenMaze>("open");

    //list every maze of the game
    std::string liste_maps[5];
    liste_maps[0] = "empty";
    liste_maps[1] = "basic";
    liste_maps[2] = "default";
    liste_maps[3] = "open";
    liste_maps[4] = "closed";

    //run simulations for each maps
    for (int nMap = 0; nMap < 5; nMap++){

        Params::mazeClassName=liste_maps[nMap];

        std::map<Role, unsigned int> results;
        results[Role::HIDER] = 0;
        results[Role::SEEKER] = 0;

        std::cout << "--------------------------------------------" << std::endl;
        std::cout<<"Simulation for "<<liste_maps[nMap]<<" maze with "<<Params::seekerClassName<<" seeker and "<<Params::hiderClassName<<" hider"<<std::endl;
        std::cout<<"Simulating "<<Params::nGames<<" games..." <<std::endl;

        for (int i = 0; i < Params::nGames; i++) {

            // create Game and init maze (required call build)
            auto maze = MazeFactory::create(liste_maps[nMap], GameParams::width, GameParams::height);
            maze->build();

            Game game(maze, GameParams::nRounds);

            // then add Players (hiders and seekers)
            for (unsigned int j = 0; j < GameParams::nHiders; j++) {

                std::string hider_name = "H" + std::to_string(j + 1);
                // only need the player representation server side
                std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
                game.addPlayer(hider);
            }

            for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

                std::string seeker_name = "S" + std::to_string(j + 1);
                // only need the player representation server side
                std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
                game.addPlayer(seeker);
            }

            // before running
            if (liste_maps[nMap] != "basic" && !Helper::isValid(game) )  {
                std::cout << "Game is not valid: unexpected maze "<<liste_maps[i]<<" built..." << std::endl;
                exit(0);
            }

            // run the game
            while (!game.end()) {

                auto player = game.getCurrentPlayer();

                // state is composed of <Observation, vector<PlayerAction>>
                auto state = game.getState(player);
                // get the chosen action by IA
                PlayerAction chosenAction = player->play(state.first, state.second);

                //To understand segmentation fault
                /*
                std::cout<<maze->toString()<<std::endl;
                std::cout<< "Joueur : " << player->getName() << "Action : "<<actionToString(chosenAction)<<std::endl;
                */

                // Play the action in the game and retrieve the new game state
                auto observation = game.step(player, chosenAction);
                // update the IA
                player->update(state.first, observation, chosenAction);
            }

            Role winner = game.getCurrentWinner();

            // update stat
            results[winner] += 1;
        }
        std::cout<<std::endl;
        std::cout <<"Results over " << Params::nGames << " games are: " << std::endl;
        for (auto &k : results) {
            std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins (" 
                    << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
        }
    }
}