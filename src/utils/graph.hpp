#ifndef HIDE_AND_SEEK_GRAPH_HPP
#define HIDE_AND_SEEK_GRAPH_HPP

#include <memory>
#include <list>

#include "maze/cell.hpp"
#include "maze/maze.hpp"


class Graph
{

public:
	Graph(const std::shared_ptr<Maze> &maze); // build graph from Maze representation

	// Use of BFS traversal from a given cell to another one
    // enable to check if path exists
	bool findPath(const std::shared_ptr<MazeCell> &from, const std::shared_ptr<MazeCell> &to);

private:
	// function to add an edge to graph
	void addEdge(const std::shared_ptr<MazeCell> &v, const std::shared_ptr<MazeCell> &w);

	// Pointer to an array containing adjacency
	std::map<std::shared_ptr<MazeCell>, std::list<std::shared_ptr<MazeCell>>> adj;
};

#endif // HIDE_AND_SEEK_GRAPH_HPP
