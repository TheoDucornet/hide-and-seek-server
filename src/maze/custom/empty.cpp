#include "maze/custom/empty.hpp"

#include "maze/block/block.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"


/**
 * @brief Default random maze
 * 
 * Empty maze to get a better view of players textures
 * 
 */

void EmptyMaze::build() {

    //Useful when maze is rebuilt by seeker
    cells.clear();
    
    // Build an empty Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            std::shared_ptr<Block> block;

            // add border
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
            } else {
                // add ground
                block = std::make_shared<Ground>();
            }
            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }
}
