#ifndef HIDE_AND_SEEK_DEFAULT_MAZE_HPP
#define HIDE_AND_SEEK_DEFAULT_MAZE_HPP

#include "maze/maze.hpp"

/**
 * @brief Simple Maze implementation:
 */
class DefaultMaze : public Maze
{
public:
    DefaultMaze(unsigned int width, unsigned int height): Maze(width, height) {};

    virtual void build();

    ~DefaultMaze() {};
};


#endif