#ifndef HIDE_AND_SEEK_EMPTY_MAZE_HPP
#define HIDE_AND_SEEK_EMPTY_MAZE_HPP

#include "maze/maze.hpp"

/**
 * @brief Empty Maze used to help for test
 */

class EmptyMaze : public Maze
{
public:
    EmptyMaze(unsigned int width, unsigned int height): Maze(width, height) {};

    virtual void build();

    ~EmptyMaze() {};
};


#endif