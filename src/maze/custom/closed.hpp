#ifndef HIDE_AND_SEEK_CLOSED_MAZE_HPP
#define HIDE_AND_SEEK_CLOSED_MAZE_HPP

#include "maze/maze.hpp"

/**
 * @brief Advanced Maze implementation which goal is to help hiders.
 * 
 * Closed Maze create many almost closed areas, better for hiders who try to hide by moving blocks
 */
class ClosedMaze : public Maze
{
public:
    ClosedMaze(unsigned int width, unsigned int height): Maze(width, height) {};

    virtual void build();

    ~ClosedMaze() {};
};


#endif