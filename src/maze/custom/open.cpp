#include "maze/custom/open.hpp"

#include "maze/block/block.hpp"

//to access game parameters
#include "game.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"

//to test every path in the maze
#include "utils/graph.hpp"

/**
 * @brief Advanced Maze
 * 
 * Still respect contraints defined in default maze,
 * but help hiders by generating lines, to block seeker's vision.
 * 
 * The open version place a 1st block as ground so the line let two exits.
 */

void OpenMaze::build() {

    // Number of Blocks & Materials
    double totalBlocks = getHeight() * getWidth();
    unsigned int nBlocks = totalBlocks * GameParams::pctBlock;
    unsigned int nWood = totalBlocks * GameParams::pctWood;
    unsigned int nStone = totalBlocks * GameParams::pctStone;
    unsigned int nMetal = totalBlocks * GameParams::pctMetal;
    
    // Build an empty Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            std::shared_ptr<Block> block;

            // add border
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
                nBlocks--;
            } else {
                // add ground
                block = std::make_shared<Ground>();
            }
            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    // Add blocks into the maze
    int cellIndex = 0;
    int direction = 0;
    int height = GameParams::height;
    int width = 1;

    while (nBlocks > 0){

        //--CELL SELECTION

        //if there is no line to build, select a random spot near a block to start a line
        if (direction == 0){
            int voisins = 0;

            while (direction == 0){
                int randomHeight = rGenerator->getRandomInt(getHeight()-1);
                int randomWidth = rGenerator->getRandomInt(getWidth()-1);
                cellIndex = (randomHeight * getWidth()) + randomWidth;

                //Check if position is near blocks and memorize where to continue the line
                if(cellIndex+height < (GameParams::height*GameParams::width-1)){
                    if(cells[cellIndex+height]->getBlock()->getKind() == BORDER || cells[cellIndex+height]->getBlock()->getKind() == MATERIAL){
                        voisins++;
                        direction = -height;
                    }
                }

                if(cellIndex-height > 0){
                    if(cells[cellIndex-height]->getBlock()->getKind() == BORDER || cells[cellIndex-height]->getBlock()->getKind() == MATERIAL){
                        voisins++;
                        direction = height;
                    }
                }

                if(cellIndex+width < (GameParams::height*GameParams::width-1)){
                    if(cells[cellIndex+width]->getBlock()->getKind() == BORDER || cells[cellIndex+width]->getBlock()->getKind() == MATERIAL){
                        voisins++;
                        direction = -width;
                    }
                }

                if(cellIndex-width > 0){
                    if(cells[cellIndex-width]->getBlock()->getKind() == BORDER || cells[cellIndex-width]->getBlock()->getKind() == MATERIAL){
                        voisins++;
                        direction = width;
                    }
                }

                //Accept only ground cell next to one block
                if (cells[cellIndex]->getBlock()->getKind() != GROUND || voisins != 1){
                    direction = 0;
                    voisins = 0;
                }
            }
            //We let the 1st block as ground to let several exits
            continue;
        }
        //else it must place the block to continue the line
        else {
            cellIndex = cellIndex + direction;
            //we need to avoid placing near an another line
            if(direction == height || direction == -height){
                if(cells[cellIndex+width]->getBlock()->getKind() != GROUND || cells[cellIndex-width]->getBlock()->getKind() != GROUND){
                    direction = 0;
                    continue;
                }
            }
            if(direction == width || direction == -width){
                if(cells[cellIndex+height]->getBlock()->getKind() != GROUND || cells[cellIndex-height]->getBlock()->getKind() != GROUND){
                   direction = 0;
                    continue;
                }
            }
        }

        //but if the position would create a closed area, placement must be random again
        if (cells[cellIndex+direction]->getBlock()->getKind() == BORDER || cells[cellIndex+direction]->getBlock()->getKind() == MATERIAL){
            direction = 0;
            continue;
        }

        //--BLOCK TYPE SELECTION

        //if place is ground, add a random block
        if(cells[cellIndex]->getBlock()->getKind() == GROUND){

            std::shared_ptr<Block> block;
            
            //if we can't place a block other than material and keep proportions, choose from materials
            unsigned int totalMaterials = nWood + nStone + nMetal;

            if(totalMaterials >= nBlocks){

                unsigned int randomMaterial = rGenerator->getRandomIntInterval(1,totalMaterials);
                
                if(randomMaterial <= nWood){
                    block = std::make_shared<Wood>();
                    nWood--;
                }
                else if (randomMaterial <= (nWood+nStone)){
                    block = std::make_shared<Stone>();
                    nStone--;
                }
                else {
                    block = std::make_shared<Metal>();
                    nMetal--;
                }

            }
            //else place a random type of block
            else{

                int randomMaterial = rGenerator->getRandomInt(3);

                if(randomMaterial == 0 && nWood > 0){
                    block = std::make_shared<Wood>();
                    nWood--;

                }
                else if (randomMaterial == 1 && nStone > 0){
                    block = std::make_shared<Stone>();
                    nStone--;

                }
                else if (randomMaterial == 2 && nMetal > 0){
                    block = std::make_shared<Metal>();
                    nMetal--;
                }
                else{
                    block = std::make_shared<Border>();
                }
            }
            
            cells[cellIndex]->setBlock(block);
            nBlocks--;
        }
    }

    //--CHECK VALIDITY OF MAZE
    
    //If there is a closed area, maze must be reseted

    // Use of a generated graph from this maze
    Graph graph(shared_from_this());

    // Get 1st available position
    std::shared_ptr<MazeCell> firstCell = nullptr;
    auto it = cells.begin();
    while(firstCell == nullptr){
        if((*it)->getBlock()->getKind() == GROUND){
            firstCell = (*it);
        }
        it++;
    }

    // Check if path exists for one position to every other positions
    for (auto testedCell = cells.begin(); testedCell != cells.end();testedCell++){

        if((*testedCell)->getBlock()->getKind() == GROUND && (*testedCell) != firstCell){

            bool hasPath = graph.findPath(firstCell,(*testedCell));

            if(!hasPath){

                //If needed, display cells not connected
                /*
                auto firstPosition = find(cells.begin(), cells.end(), firstCell);
                int firstIndex = firstPosition - cells.begin();
                auto testedPosition = find(cells.begin(),cells.end(), (*testedCell));
                int testedIndex = testedPosition - cells.begin();

                std::cout<<"Labyrinthe non valide : Problème entre position "<<firstIndex<<" et "<<testedIndex<<std::endl;
                std::cout<<toString();
                */

                //Delete and regenerate maze
                cells.clear();
                build();

                break;
            }
        }
    }
}
