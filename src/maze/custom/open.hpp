#ifndef HIDE_AND_SEEK_OPEN_MAZE_HPP
#define HIDE_AND_SEEK_OPEN_MAZE_HPP

#include "maze/maze.hpp"

/**
 * @brief Advanced Maze implementation which goal is to help hiders.
 * 
 * Open Maze create areas with more exits to help hider run away.
 */

class OpenMaze : public Maze
{
public:
    OpenMaze(unsigned int width, unsigned int height): Maze(width, height) {};

    virtual void build();

    ~OpenMaze() {};
};


#endif