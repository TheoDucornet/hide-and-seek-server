#include "maze/custom/default.hpp"

#include "maze/block/block.hpp"

//to access game parameters
#include "game.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"

//to test every path in the maze
#include "utils/graph.hpp"

/**
 * @brief Default random maze
 * 
 * Percentage of blocks and type of materials depend of Game Params (in game.hpp)
 * 
 * Maze must be generated without closed area
 */

void DefaultMaze::build() {

    // Number of Blocks & Materials
    double totalBlocks = getHeight() * getWidth();
    unsigned int nBlocks = totalBlocks * GameParams::pctBlock;
    unsigned int nWood = totalBlocks * GameParams::pctWood;
    unsigned int nStone = totalBlocks * GameParams::pctStone;
    unsigned int nMetal = totalBlocks * GameParams::pctMetal;
    
    // Build an empty Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            std::shared_ptr<Block> block;

            // add border
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
                nBlocks--;
            } else {
                // add ground
                block = std::make_shared<Ground>();
            }
            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    // Add blocks into the maze
    while (nBlocks > 0){

        //get a random cell
        int randomHeight = rGenerator->getRandomInt(getHeight()-1);
        int randomWidth = rGenerator->getRandomInt(getWidth()-1);
        unsigned int cellIndex = (randomHeight * this->width) + randomWidth;

        //if place is ground, add a random block
        if(cells[cellIndex]->getBlock()->getKind() == GROUND){

            std::shared_ptr<Block> block;
            
            //if we can't place a block other than material and keep proportions, choose from materials
            unsigned int totalMaterials = nWood + nStone + nMetal;

            if(totalMaterials >= nBlocks){

                unsigned int randomMaterial = rGenerator->getRandomIntInterval(1,totalMaterials);
                
                if(randomMaterial <= nWood){
                    block = std::make_shared<Wood>();
                    nWood--;
                }
                else if (randomMaterial <= (nWood+nStone)){
                    block = std::make_shared<Stone>();
                    nStone--;
                }
                else {
                    block = std::make_shared<Metal>();
                    nMetal--;
                }

            }
            //else place a random type of block
            else{

                int randomMaterial = rGenerator->getRandomInt(3);

                if(randomMaterial == 0 && nWood > 0){
                    block = std::make_shared<Wood>();
                    nWood--;

                }
                else if (randomMaterial == 1 && nStone > 0){
                    block = std::make_shared<Stone>();
                    nStone--;

                }
                else if (randomMaterial == 2 && nMetal > 0){
                    block = std::make_shared<Metal>();
                    nMetal--;
                }
                else{
                    block = std::make_shared<Border>();
                }
            }
            
            cells[cellIndex]->setBlock(block);
            nBlocks--;
        }
    }

    
    //If there is a closed area, maze must be reseted

    // Use of a generated graph from this maze
    Graph graph(shared_from_this());

    // Get 1st available position
    std::shared_ptr<MazeCell> firstCell = nullptr;
    auto it = cells.begin();
    while(firstCell == nullptr){
        if((*it)->getBlock()->getKind() == GROUND){
            firstCell = (*it);
        }
        it++;
    }

    // Check if path exists for one position to every other positions
    for (auto testedCell = cells.begin(); testedCell != cells.end();testedCell++){

        if((*testedCell)->getBlock()->getKind() == GROUND && (*testedCell) != firstCell){

            bool hasPath = graph.findPath(firstCell,(*testedCell));

            if(!hasPath){

                //If needed, display cells not connected
                /*
                auto firstPosition = find(cells.begin(), cells.end(), firstCell);
                int firstIndex = firstPosition - cells.begin();
                auto testedPosition = find(cells.begin(),cells.end(), (*testedCell));
                int testedIndex = testedPosition - cells.begin();

                std::cout<<"Labyrinthe non valide : Problème entre position "<<firstIndex<<" et "<<testedIndex<<std::endl;
                std::cout<<toString();
                */

                //Delete and regenerate maze
                cells.clear();
                build();

                break;
            }
        }
    }
    

}
