#ifndef HIDE_AND_SEEK_BLOCK_BORDER_HPP
#define HIDE_AND_SEEK_BLOCK_BORDER_HPP

#include <string>

#include "maze/block/block.hpp"
#include "maze/block/kind.hpp"

class Border : public Block
{
public:
    Border() : Block() {};

    virtual BKind getKind() const {
        return BKind::BORDER;
    };

    virtual std::string getMaterial() const {
        return "";
    }

    virtual void interact() {
        // nothing happened with border...
    };

    ~Border() {};
};

#endif