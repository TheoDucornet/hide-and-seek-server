#include "maze/block/block.hpp"

#include "maze/block/kind.hpp"
#include "maze/block/material/material.hpp"
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/undefined.hpp"

/**
 * @brief Specific json output of the hider
 * 
 * @return nlohmann::json
 */
nlohmann::json Block::toJSON() const {
    
    // call mother method and add some specification
    nlohmann::json j;

    j["resistance"] = this->resistance;
    j["available"] = this->available;
    j["movable"] = this->movable;
    j["kind"] = this->getKind();
    j["kindStr"] = blockKindToStringForJSON(this->getKind());
    
    return j;
}


std::shared_ptr<Block> Block::fromJSON(const nlohmann::json &data) {

    std::shared_ptr<Block> block;

    BKind kind = data["kind"].get<BKind>();

    // no need of more information, default block always available and not movable
    switch (kind)
    {
    case BORDER:
        return std::make_shared<Border>();
        break;
    case GROUND:
        return std::make_shared<Ground>();
        break;
    case MATERIAL:
        return Material::fromJSON(data);
        break;   
    case UNDEFINED:
        return std::make_shared<Undefined>();
        break;         
    default:
        return std::make_shared<Undefined>();
        break;
    }
}