#ifndef HIDE_AND_SEEK_BLOCK_MATERIAL_CUSTOM_HPP
#define HIDE_AND_SEEK_BLOCK_MATERIAL_CUSTOM_HPP

#include <string>

#include "maze/block/material/material.hpp"
#include "maze/block/kind.hpp"

/**
 * @brief Custom Wood class
 * 
 */
class Wood : public Material
{
public:
    Wood() : Material(100, true) {};
    Wood(int resistance, bool available, int hitDamage, bool movable) : Material(resistance, available, hitDamage, movable) {};

    virtual std::string getMaterial() const {
        return "W";
    }

    virtual MaterialKind getMaterialKind() const {
        return MaterialKind::WOOD;
    }
};

/**
 * @brief Custom Stone class
 * 
 */
class Stone : public Material
{
public:
    Stone() : Material(50, true) {};

    Stone(int resistance, bool available, int hitDamage, bool movable) : Material(resistance, available, hitDamage, movable) {};

    virtual std::string getMaterial() const {
        return "S";
    }

    virtual MaterialKind getMaterialKind() const {
        return MaterialKind::STONE;
    }
};

/**
 * @brief Custom Metal class
 * 
 */
class Metal : public Material
{
public:
    Metal() : Material(20, false) {};
    Metal(int resistance, bool available, int hitDamage, bool movable) : Material(resistance, available, hitDamage, movable) {};

    virtual std::string getMaterial() const {
        return "M";
    }

    virtual MaterialKind getMaterialKind() const {
        return MaterialKind::METAL;
    }
};


#endif