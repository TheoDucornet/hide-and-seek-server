#ifndef HIDE_AND_SEEK_REST_SERVER_HPP
#define HIDE_AND_SEEK_REST_SERVER_HPP

#include "utils/json.hpp"
#include <utils/httplib.hpp>
#include <memory>
#include <mutex>   

#include "game.hpp"
#include "utils/helper.hpp"
#include "player/action.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"
#include "maze/block/material/material.hpp"

std::mutex mtx;  

class Counter
{
    public:
        static size_t value;

        static size_t uniqueID()
        {
            Counter::value++;
            return value;
        }
};


size_t Counter::value = 0;


class GameServer
{
public:
    GameServer() {}

    void init() {
        // Register the basic kinds of Player and Maze, just for the representation
        HiderFactory::registerClass<Hider>("hider");
        SeekerFactory::registerClass<Seeker>("seeker");
        MazeFactory::registerClass<Maze>("maze");
        
        setupRoutes();
    }

    void start(int port, const std::string &address)
    {  
        std::cout << "Server listen on: " << address << ":" << port << std::endl;
        svr.listen(address.c_str(), port);
    }

    void stop() {

        std::cout << "Server will be disconnected... Each client will be noticed..." << std::endl;
        for (auto &client : clients) {

            httplib::Client cli(client->getAddress(), client->getPort());
            cli.Post("/disconnect");
        }
    }

private:
    void setupRoutes()
    {
        svr.Get("/", [this](const httplib::Request &, httplib::Response &res) {
            res.set_content("Hello World!", "text/plain");
        });

        svr.Post("/connect", [this](const httplib::Request &req, httplib::Response &res) {

            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);
                std::cout << "[/connect] Received data: " << json_data << std::endl;

                auto port = json_data["port"].get<int>();
                auto address = json_data["address"].get<std::string>();
                auto role = json_data["role"].get<Role>();
                auto className = json_data["className"].get<std::string>();

                auto it = std::find_if(clients.begin(), clients.end(), [&port, &address](auto &cli){
                    return port == cli->getPort() && address == cli->getAddress();
                });

                if (it == clients.end()) {
                    auto cli = std::make_shared<ClientPlayer>(port, address, className, role);
                    clients.push_back(cli);

                    std::cout << "-- New connected client: " << cli->getURL() << std::endl;
                    res.status = 200;
                    res.set_content("Connection OK", "text/plain");  
                } 
                else {
                    std::cout << "-- Client attempted to connect twice: [" << address << ", " << port << "]" << std::endl;
                    res.status = 406;
                    res.set_content("Address and port already in usage...", "text/plain");  
                }

            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/create", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("Access-Control-Allow-Origin", "*");

            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "[/game/create] Received data: " << json_data << std::endl;

                if (json_data.contains("hider") && json_data.contains("seeker")) {

                    auto hiderConfig = json_data["hider"].get<nlohmann::json>();
                    auto seekerConfig = json_data["seeker"].get<nlohmann::json>();

                    auto hiderIt = std::find_if(clients.begin(), clients.end(), [&hiderConfig] (const auto &cli) {
                        int port = hiderConfig["port"].get<int>();
                        std::string address = hiderConfig["address"].get<std::string>();
                        Role role = hiderConfig["role"].get<Role>();

                        return cli->getRole() == role && cli->getAddress().compare(address) == 0 && cli->getPort() == port;
                    });

                    auto seekerIt = std::find_if(clients.begin(), clients.end(), [&seekerConfig] (const auto &cli) {
                        int port = seekerConfig["port"].get<int>();
                        std::string address = seekerConfig["address"].get<std::string>();
                        Role role = seekerConfig["role"].get<Role>();

                        return cli->getRole() == role && cli->getAddress().compare(address) == 0 && cli->getPort() == port;
                    });

                    if (hiderIt != clients.end() && seekerIt != clients.end()) {

                        int gameId = Counter::uniqueID();

                        // init Game with Maze and built it
                        httplib::Client hiderHttpCli((*hiderIt)->getURL());

                        nlohmann::json maze_data;

                        maze_data["game"] = gameId;
                        maze_data["width"] = GameParams::width;
                        maze_data["height"] = GameParams::height;

                        // Ask Hider to send Maze with specific size
                        if (auto resHider = hiderHttpCli.Post("/game/maze", maze_data.dump(), "application/json")) {

                            if (resHider->status == 200) {
                                
                                nlohmann::json generated_maze_data = nlohmann::json::parse(resHider->body);
                                
                                if (generated_maze_data.contains("maze")) {
                                    
                                    // basic use of from JSON Maze
                                    auto maze = MazeFactory::fromJSON("maze", generated_maze_data["maze"]);

                                    auto game = std::make_shared<Game>(maze, GameParams::nRounds, *hiderIt, *seekerIt);

                                    try {
                                        // then add Players (hiders and seekers)
                                        for (unsigned int i = 0; i < GameParams::nHiders; i++) {
                                            
                                            std::string hider_name = "H" + std::to_string(i + 1);
                                            // only need the player representation server side
                                            std::shared_ptr<Player> hider = HiderFactory::create("hider", hider_name);
                                            game->addPlayer(hider);
                                        }

                                        for (unsigned int i = 0; i < GameParams::nSeekers; i++) {
                                            
                                            std::string seeker_name = "S" + std::to_string(i + 1);
                                            // only need the player representation server side
                                            std::shared_ptr<Player> seeker = SeekerFactory::create("seeker", seeker_name);
                                            game->addPlayer(seeker);
                                        }

                                        if (Helper::isValid(*game)) {

                                            // only if it's valid...
                                            std::cout << "New created game with {id: " << gameId 
                                                << ", hider: " << (*hiderIt)->getClassName()
                                                << ", seeker: " << (*seekerIt)->getClassName()
                                                << ", maze: " << "default}" << std::endl;

                                            games.insert({gameId, game});

                                            // Send data players representation from created game to each Client
                                            auto hiderPlayers = game->getPlayers(Role::HIDER);
                                            auto seekerPlayers = game->getPlayers(Role::SEEKER);

                                            // send data to client
                                            auto hiderClient = game->getClient(Role::HIDER);
                                            auto seekerClient = game->getClient(Role::SEEKER);

                                            nlohmann::json seeker_data;
                                            nlohmann::json hider_data;
                                            hider_data["game"] = gameId;
                                            seeker_data["game"] = gameId;

                                            hider_data["className"] = hiderClient->getClassName();
                                            seeker_data["className"] = seekerClient->getClassName();

                                            nlohmann::json hider_players;
                                            for (auto &p : hiderPlayers)
                                                hider_players.push_back(p->toJSON());
                                            hider_data["players"] = hider_players;

                                            nlohmann::json seeker_players;
                                            for (auto &p : seekerPlayers) {
                                                seeker_players.push_back(p->toJSON());
                                            }
                                            seeker_data["players"] = seeker_players;

                                            httplib::Client hiderHttpCli(hiderClient->getURL());
                                            hiderHttpCli.Post("/game", hider_data.dump(), "application/json");

                                            httplib::Client seekerHttpCli(seekerClient->getURL());
                                            seekerHttpCli.Post("/game", seeker_data.dump(), "application/json");

                                            auto json_game = game->toJSON();
                                            json_game["id"] = gameId;

                                            // return the first game state to front
                                            res.status = 200;
                                            res.set_content(json_game.dump(), "application/json; charset=utf-8");
                                        }
                                        else {
                                            std::cout << "Invalid generated maze" << std::endl;
                                            res.status = 406;
                                            res.set_content("Invalid generated maze", "text/plain");
                                        }

                                    } catch(ClassNotFound& e) {
                                    
                                        std::cout << e.what() << std::endl;
                                        res.status = 406;
                                        res.set_content(e.what(), "text/plain");
                                    }
                                }
                                else {
                                    std::cout << "Error while parsing received maze" << std::endl;
                                    res.status = 406;
                                    res.set_content("Maze for the game cannot be created", "text/plain");
                                }
                            } 
                            else {
                                std::cout << "Error while creating the maze: " << resHider->body << std::endl;

                                res.status = 406;
                                res.set_content("Maze for the game cannot be created", "text/plain");
                            }
                        }
                        else {
                            res.status = 406;
                            res.set_content("Maze for the game cannot be created", "text/plain");
                        }
                    }
                    else {
                        res.status = 406;
                        res.set_content("Possible asked client not found", "text/plain");
                    }
                }
                else {
                    res.status = 406;
                    res.set_content("Missing parameters...", "text/plain");
                }
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/step", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("Access-Control-Allow-Origin", "*");

            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game/step] Received data: " << json_data << std::endl;

                if (json_data.contains("id")) {

                    try {
                        auto gameId = json_data["id"].get<int>();

                        auto element = games.find(gameId);

                        if (element != games.end() && !element->second->end()) {

                            auto game = element->second;

                            std::cout << "------" << std::endl;
                            std::cout << "Game id: " << gameId << std::endl;
                            std::cout << "-- Round n°" << game->getNumberOfRounds() << std::endl;

                            auto player = game->getCurrentPlayer();
                            std::cout << "-- Player " << *player << " has to play." << std::endl;

                            auto state = game->getState(player);

                            // prepare request data
                            nlohmann::json json_step;
                            json_step["game"] = gameId;
                            json_step["round"] = game->getNumberOfRounds();
                            json_step["maxRounds"] = GameParams::nRounds;
                            
                            nlohmann::json json_player = player->toJSON();
                            json_player["observation"] = state.first->toJSON();
                            
                            std::list<nlohmann::json> actions;

                            for (auto a : state.second) {
                                nlohmann::json action = a.toJSON();
                                actions.push_back(action);
                            }

                            json_player["possibleActions"] = actions;
                            json_step["player"] = json_player;

                            // send information to client
                            auto pClient = game->getClient(player->getRole());

                            httplib::Client pHttpCli(pClient->getURL());
                            pHttpCli.set_connection_timeout(0, 300000); // 300 MS
                            pHttpCli.set_read_timeout(1, 0); // 1 second
                            pHttpCli.set_write_timeout(1, 0); // 1 second

                            auto playerRes = pHttpCli.Post("/game/step", json_step.dump(), "application/json");

                            // check also timeout...
                            if (playerRes == nullptr || playerRes->status == 200) {
                                
                                PlayerAction chosenAction;

                                // depending of timeout or not...
                                if (playerRes == nullptr) {

                                    // default random action
                                    std::cout << "[Timeout]: Random action..." << std::endl;

                                    // Retrieve the current random generator
                                    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
                                    chosenAction = state.second[rGenerator->getRandomInt(actions.size() - 1)];
                                }
                                else {
                                    
                                    // classic answer
                                    std::cout << "selected action" << std::endl ;
                                    nlohmann::json json_answer = nlohmann::json::parse(playerRes->body);
                                    chosenAction = PlayerAction::fromJSON(json_answer);                                    
                                }

                                auto observation = game->step(player, chosenAction);
                                std::cout << *game->getMaze() << std::endl ;
                                // prepare data to player client
                                nlohmann::json observation_json;

                                observation_json["game"] = gameId;
                                observation_json["player"] = player->toJSON();
                                observation_json["previous"] = state.first->toJSON();
                                observation_json["current"] = observation->toJSON();
                                observation_json["action"] = chosenAction.toJSON();

                                pHttpCli.Post("/game/update", observation_json.dump(), "application/json");

                                std::cout << "-- " << *player << " choose action: " << actionToString(chosenAction) << std::endl;

                                // imply hider and material block
                                if (Interaction::CARRY == chosenAction.interaction) {

                                    // get current hider and material block
                                    auto hider = std::dynamic_pointer_cast<Hider>(player);
                                    auto materialBlock = std::dynamic_pointer_cast<Material>(hider->getBlock());

                                    std::cout << "-- " << *player << " carry: " << materialKindToString(materialBlock->getMaterialKind()) << std::endl;
                                }

                                nlohmann::json output_data = element->second->toJSON();
                                output_data["id"] = gameId;
                                output_data["currentPlayer"] = player->toJSON();
                                output_data["currentAction"] = chosenAction.toJSON();
                                output_data["previousObservation"] = state.first->toJSON();
                                output_data["currentObservation"] = observation->toJSON();

                                if (game->end()) {
                                    std::cout << "-- game is finished " << std::endl;

                                    // prevent other player of the end of game
                                    auto hiderClient = game->getClient(Role::HIDER);
                                    auto seekerClient = game->getClient(Role::SEEKER);

                                    output_data["winner"] = game->getCurrentWinner();
                                    output_data["winnerStr"] = roleToString(game->getCurrentWinner());

                                    httplib::Client hiderHttpCli(hiderClient->getURL());
                                    hiderHttpCli.Post("/game/end", output_data.dump(), "application/json");

                                    httplib::Client seekerHttpCli(seekerClient->getURL());
                                    seekerHttpCli.Post("/game/end", output_data.dump(), "application/json");

                                    // erase the current game
                                    games.erase(gameId);
                                }

                                // return the last game state
                                res.status = 200;
                                res.set_content(output_data.dump(), "application/json; charset=utf-8");
                            }
                            else {

                                std::cout << playerRes->body << std::endl;
                                res.status = 406;
                                res.set_content("Error occur when getting client answer", "text/plain");  
                            }
                        }
                        else {
                            res.status = 406;
                            res.set_content("Game not found...", "text/plain");
                        }
                    }
                    catch(std::exception &e){
                        std::cout << e.what() << std::endl;
                        res.status = 406;
                        res.set_content("Invalid id parameter", "text/plain");   
                    }

                } else {
                    res.status = 406;
                    res.set_content("Missing parameter game id...", "text/plain");
                } 
            }
                catch(std::exception &e){
                    std::cout << e.what() << std::endl;
                    res.status = 406;
                    res.set_content("Invalid id parameter", "text/plain");   
                }  
        });

        svr.Get("/game", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("Access-Control-Allow-Origin", "*");

            if (req.has_param("id")) {

                try {
                    auto gameId = std::stoi(req.get_param_value("id"));

                    auto element = games.find(gameId);

                    if (element != games.end()) {

                        // return the found game
                        res.status = 200;
                        res.set_content(element->second->toJSON().dump(), "application/json; charset=utf-8");
                    }
                    else {
                        res.status = 406;
                        res.set_content("Game not found...", "text/plain");   
                    }
                }
                catch(std::exception &e){
                    res.status = 406;
                    res.set_content("Invalid id parameter", "text/plain");   
                }

            } else {
                res.status = 406;
                res.set_content("Missing parameter game id...", "text/plain");
            }   
        });

        svr.Get("/games", [this](const httplib::Request &req, httplib::Response &res) {
            nlohmann::json jsonGames;

            for (const auto& g : games) {

                nlohmann::json currentGame;

                currentGame["id"] = g.first;
                currentGame["nplayers"] = g.second->getNumberOfPlayers();
                currentGame["round"] = g.second->getNumberOfRounds();
                currentGame["mazeWidth"] = g.second->getMaze()->getWidth();
                currentGame["mazeHeight"] = g.second->getMaze()->getHeight();

                jsonGames.push_back(currentGame);
            }

            res.status = 200;
            res.set_header("Access-Control-Allow-Origin", "*");
            res.set_content(jsonGames.dump(), "application/json; charset=utf-8");
        });

        svr.Get("/clients", [this](const httplib::Request &req, httplib::Response &res) {
            nlohmann::json jsonClients;

            for (const auto& cli : clients) {

                nlohmann::json currentClient;

                currentClient["address"] = cli->getAddress();
                currentClient["port"] = cli->getPort();
                currentClient["role"] = cli->getRole();
                currentClient["roleStr"] = roleToString(cli->getRole());
                currentClient["className"] = cli->getClassName();

                std::vector<int> gamesIds;

                for (auto &g : games) {

                    if (g.second->getClient(cli->getRole()) == cli) {
                        gamesIds.push_back(g.first);
                    }
                
                }
                currentClient["games"] = gamesIds;

                jsonClients.push_back(currentClient);
            }

            res.status = 200;
            res.set_header("Access-Control-Allow-Origin", "*");
            res.set_content(jsonClients.dump(), "application/json; charset=utf-8");
        });

        svr.Post("/disconnect", [this](const httplib::Request &req, httplib::Response &res) {
            
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                mtx.lock();
                // find and remove the client (and its associated games)
                auto it = std::find_if(clients.begin(), clients.end(), [&json_data] (const auto &cli) {
                    int port = json_data["port"].get<int>();
                    std::string address = json_data["address"].get<std::string>();
                    Role role = json_data["role"].get<Role>();

                    return cli->getRole() == role && cli->getAddress().compare(address) == 0 && cli->getPort() == port;
                });

                if (it != clients.end()) {

                    std::cout << "Client disconnected: " << json_data << std::endl;
                    
                    // find all associated games
                    std::vector<int> keys;

                    for (auto &game : games) {
                        auto gameClient = game.second->getClient((*it)->getRole());

                        if (gameClient == *it) {
                            keys.push_back(game.first);
                        }
                    }

                    // remove all games
                    for (auto &k : keys)
                        games.erase(k);

                    clients.erase(it);
                }
                mtx.unlock();

                res.set_header("Access-Control-Allow-Origin", "*");
                res.set_content("Bye bye...", "text/plain");

            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });
    }

    std::map<int, std::shared_ptr<Game>> games;
    std::vector<std::shared_ptr<ClientPlayer>> clients;
    httplib::Server svr;
};

#endif // HIDE_AND_SEEK_REST_SERVER