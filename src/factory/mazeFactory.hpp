#ifndef HIDE_AND_SEEK_MAZE_FACTORY_HPP
#define HIDE_AND_SEEK_MAZE_FACTORY_HPP

#include <memory>
#include <string>
#include <vector>
#include <type_traits>

#include "maze/maze.hpp"

#include "utils/exceptions.hpp"

/**
 * @brief Factory for Maze
 * Need to register class
 * 
 */
class MazeFactory
{
public:
    using Instantiate = std::function<std::shared_ptr<Maze>(int, int)>;
    using MapType = std::map<std::string, Instantiate>;
        
    using From = std::function<std::shared_ptr<Maze>(const nlohmann::json &)>;
    using MapFrom = std::map<std::string, From>;
 
    static std::shared_ptr<Maze> create(std::string const& className, int width, int height) {

        // try to find the registered function
        auto it = types.find(className);
        if(it == types.end())
            return nullptr;
        return it->second(width, height);
    }

    static std::shared_ptr<Maze> fromJSON(std::string const& className, const nlohmann::json &data) {

        // try to find the registered function
        auto it = origins.find(className);
        if(it == origins.end())
            return nullptr;
        return it->second(data);
    }

    static bool isRegistered(const std::string &className) {
        
        for(auto it = types.begin(); it != types.end(); ++it) {
            if (it->first == className)
                return true;
        }
        return false;
    }

    template<typename T>
    static std::enable_if_t<std::is_base_of<Maze, T>::value, void>
    registerClass(const std::string &className) {
        
        // replace by default the key...
        types[className] = &MazeFactory::create<T>;
        origins[className] = &MazeFactory::fromJSON<T>;
    }

    protected:
        // register create method for hiders and seekers
        static MapType types;
        static MapFrom origins;

        // template method in order to create Hider or Seeker
        template<typename T>
        static std::enable_if_t<std::is_base_of<Maze, T>::value, std::shared_ptr<T>>
        create(int width, int height) {
            
            return std::make_shared<T>(width, height);
        }

        template<typename T>
        static std::enable_if_t<std::is_base_of<Maze, T>::value, std::shared_ptr<T>>
        fromJSON(const nlohmann::json &data) {

            auto width = data["width"].get<int>();
            auto height = data["height"].get<int>();

            std::vector<std::shared_ptr<MazeCell>> cells;

            for (auto &c : data["cells"])
                cells.push_back(MazeCell::fromJSON(c));

            auto maze = std::make_shared<T>(width, height);
            maze->setCells(cells);

            return maze;
        };
};

MazeFactory::MapType MazeFactory::types {};
MazeFactory::MapFrom MazeFactory::origins {};

#endif

