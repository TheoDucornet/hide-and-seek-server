#include "player/seeker/random.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new RandomSeeker:: RandomSeeker object
 * 
 * 
 * @param name 
 */
RandomSeeker::RandomSeeker(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
RandomSeeker::~RandomSeeker() {}

/**
 * @brief Return a random action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &RandomSeeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {
    
    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    
    return actions[rGenerator->getRandomInt(actions.size() - 1)]; 
}


void RandomSeeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
};
