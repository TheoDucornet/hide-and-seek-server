#include "player/seeker/seeker.hpp"

/**
 * @brief Construct a new Seeker:: Seeker object
 * 
 * Specify the current seeker possible actions
 * 
 * @param name 
 */
Seeker::Seeker(const std::string &name) : Player(name){

    // list all the actions for seeker
    for (auto o : PlayerAction::getOrientations()) {
        possibleActions.push_back(PlayerAction(o, Interaction::MOVE));
        possibleActions.push_back(PlayerAction(o, Interaction::HIT));
        possibleActions.push_back(PlayerAction(o, Interaction::NOTHING));
    }
}

/**
 * @brief Specific SEEKER role 
 * 
 * @return Role 
 */
Role Seeker::getRole() const {
    return Role::SEEKER;
}

/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
Seeker::~Seeker() {}