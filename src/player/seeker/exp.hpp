#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_EXP_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_EXP_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief expert Seeker class
 * 
 */
class ExpSeeker : public Seeker
{
public:
    ExpSeeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    
    ~ExpSeeker();

    int calc_distance(int, int) const ;
};

#endif