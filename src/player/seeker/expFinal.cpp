#include "player/seeker/expFinal.hpp"
#include "utils/randomGenerator.hpp"
#include "maze/custom/empty.hpp"
#include "maze/block/undefined.hpp"
#include "maze/block/material/material.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/border.hpp"

#include "game.hpp"

#include <vector>

/**
 * @brief Construct a new ExpSeeker:: ExpSeeker object
 * 
 * 
 * @param name 
 */
ExpSeekerFinal::ExpSeekerFinal(const std::string &name) : Seeker(name){

    // create a maze
    this->maze = std::make_shared<EmptyMaze>(GameParams::width,GameParams::height);
    maze->build();

    // set all cells in maze to unknown
    for (unsigned int i = 0; i < GameParams::height; i++) {
        for (unsigned int j = 0; j < GameParams::width; j++) {
            
            unsigned int cellIndex = (i * GameParams::width) + j;

            if (maze->getCells()[cellIndex]->getBlock()->getKind() == GROUND){
                maze->getCells()[cellIndex]->setBlock(std::make_shared<Undefined>());
            }
        }
    } 
}


/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
ExpSeekerFinal::~ExpSeekerFinal() {}

int ExpSeekerFinal::calc_distance(int x1, int x2, int y1, int y2) const
{
    int diffx = x2 - x1 ;
    if(diffx < 0) { diffx = -diffx ;}
    int diffy = y2 - y1 ;
    if(diffy < 0) { diffy = -diffy ;}

    return diffx + diffy ;
}

const PlayerAction &ExpSeekerFinal::Random(const std::vector<PlayerAction> &actions) const
{
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    return actions[rGenerator->getRandomInt(actions.size() - 1)] ;
}

const PlayerAction &ExpSeekerFinal::GoTo(const std::vector<PlayerAction> &actions, int px, int py) const
{       
    int dist = calc_distance(px, position.x, py, position.y) ;
    size_t action ;
    bool isAction = false ;
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        Orientation ori = actions[n].orientation ;
        Interaction inte = actions[n].interaction ;

        if(ori == TOP && inte == MOVE ){
            if( dist > calc_distance( px, position.x, py, position.y-1 )){
                dist = calc_distance( px, position.x, py, position.y-1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == BOTTOM && inte == MOVE){
            if( dist > calc_distance( px, position.x, py, position.y+1) ){
                dist = calc_distance( px, position.x, py, position.y+1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == LEFT && inte == MOVE ){
            if( dist > calc_distance( px, position.x-1, py, position.y )){
                dist = calc_distance( px, position.x-1, py, position.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == RIGHT && inte == MOVE ){
            if( dist > calc_distance( px, position.x+1, py, position.y) ){
                dist = calc_distance( px, position.x+1, py, position.y) ;
                action = n ;
                isAction = true ;
            }}
    }
    if(isAction)
        return actions[action] ;
    else{
        return Random(actions) ;
    }
}

const PlayerAction &ExpSeekerFinal::GetDirection(const std::vector<PlayerAction> &actions) const{
    
    int Sy = position.y ;
    int Sx = position.x ;
    
    //Show destination and player position
    /*
    std::cout<<"Block à atteindre : "<<cell_to_check<<std::endl;
    std::cout<<"Player cell : "<<position.y<<" - "<<position.x<<" -> "<<position.y*GameParams::width+position.x<<std::endl;
    */

    //Get the 1st cell to go to destination
    std::shared_ptr<MazeCell> next_cell_to_go = maze->getPath(maze->getCells()[position.y*GameParams::width+position.x],maze->getCells()[cell_to_check]);  

    //Show 1st cell of the path to destination
    /*
    auto it = find(maze->getCells().cbegin(),maze->getCells().cend(),next_cell_to_go);
    std::cout<<getName()<<" cherche à atteindre : "<<it-maze->getCells().cbegin()<<" (position :"<<position.y*GameParams::width+position.x<<" )"<<std::endl;
    */
    
    //return best action to get on next cell
    for(size_t n=0 ; n <= actions.size() ; n++){
        Orientation ori = actions[n].orientation;
        Interaction inte = actions[n].interaction;
        //Priority is to move
        if(ori == TOP && maze->getCell(Sx,Sy-1) == next_cell_to_go && (inte == MOVE || inte == HIT)){
            return actions[n];
        }
        if(ori == BOTTOM && maze->getCell(Sx,Sy+1) == next_cell_to_go && (inte == MOVE || inte == HIT)){
            return actions[n];
        }
        if(ori == LEFT && maze->getCell(Sx-1,Sy) == next_cell_to_go && (inte == MOVE || inte == HIT)){
            return actions[n];
        }
        if(ori == RIGHT && maze->getCell(Sx+1,Sy) == next_cell_to_go && (inte == MOVE || inte == HIT)){
            return actions[n];
        }
        //Else seeker will just look
        if(ori == TOP && maze->getCell(Sx,Sy-1) == next_cell_to_go){
            return actions[n];
        }
        if(ori == BOTTOM && maze->getCell(Sx,Sy+1) == next_cell_to_go){
            return actions[n];
        }
        if(ori == LEFT && maze->getCell(Sx-1,Sy) == next_cell_to_go){
            return actions[n];
        }
        if(ori == RIGHT && maze->getCell(Sx+1,Sy) == next_cell_to_go){
            return actions[n];
        }
    }
    return Random(actions) ;
} ;

/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpSeekerFinal::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

        // go closer to hider
    if(HiderSeen){
        return GoTo(actions, position_Hider.x, position_Hider.y) ;
    }
        // else return the best action to get closer to unknown location
        // need to check if there is a determined unknown cell (determined in update())
    else if(cell_to_check != -1){
        return GetDirection(actions);
    }
        // if there is no destination (update has not been called or there is no unknown cell), play random
    else{
        return Random(actions) ;
    }
}

void ExpSeekerFinal::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    //Check if hider in sight 
    position = getLocation() ;
    std::vector<std::shared_ptr<MazeCell>> cells = next->getCells() ;

    if(position == position_Hider)
        HiderSeen = false ;

    for(std::shared_ptr<MazeCell> cell : cells)
        if(cell->hasPlayer()){
            if(cell->getPlayer()->getRole() == Role::HIDER)
            {
                position_Hider = cell->getPlayer()->getLocation() ;
                HiderSeen = true ;
            }
            else if(cell->getPlayer()->getRole() == Role::SEEKER)
            {
                position_Seeker = cell->getPlayer()->getLocation() ;
                SeekerSeen = true ;
            }
        }
    
    //Update maze representation with new observations
    auto observed_cells = next->getCells();

    for (unsigned int i = 0; i < GameParams::height; i++) {
        for (unsigned int j = 0; j < GameParams::width; j++) {
            
            unsigned int cellIndex = (i * GameParams::width) + j;

            if(observed_cells[cellIndex]->getPlayer() != nullptr){

                //if cell is the other seeker, count as border
                if(observed_cells[cellIndex]->getPlayer()->getRole()==SEEKER && observed_cells[cellIndex]->getPlayer()->getName() != getName())
                    maze->getCells()[cellIndex]->setBlock(std::make_shared<Border>());
                //else count as ground
                else
                    maze->getCells()[cellIndex]->setBlock(std::make_shared<Ground>());
            }
            else if (observed_cells[cellIndex]->getBlock()->getKind() != UNDEFINED){
                maze->getCells()[cellIndex]->setBlock(observed_cells[cellIndex]->getBlock());
            }
        }
    }  

    //Display maze representation of the seeker
    /*
    std::cout<<maze->toString()<<std::endl;
    */

    // If there is no destination, select the closest unkown position
    if(cell_to_check == -1 || maze->getCells()[cell_to_check]->getBlock()->getKind() != UNDEFINED){

        int minimal_distance = INT16_MAX;

        //if no unkown position, reset maze
        bool at_least_one_unknown_position = false;

        //get all undefined positions
        for (unsigned int i = 0; i < GameParams::height; i++) {
            for (unsigned int j = 0; j < GameParams::width; j++) {

                //get undefined cell closest to player
                int cellIndex = (i * GameParams::width) + j;
                int player_cell = position.y*GameParams::width+position.x;

                if (maze->getCells()[cellIndex]->getBlock()->getKind() == UNDEFINED){
                    at_least_one_unknown_position = true;
                    if (abs(player_cell - cellIndex) < minimal_distance){
                        cell_to_check = cellIndex;
                        minimal_distance = abs(player_cell - cellIndex);
                    }                    
                }
            }
        }

        //Check if we found at least one unknow position
        if(!at_least_one_unknown_position){
            //maze must be reseted to get explored again
            maze->build();

            // set all cells in maze to unknown
            for (unsigned int i = 0; i < GameParams::height; i++) {
                for (unsigned int j = 0; j < GameParams::width; j++) {

                    unsigned int cellIndex = (i * GameParams::width) + j;

                    if (maze->getCells()[cellIndex]->getBlock()->getKind() == GROUND){
                        maze->getCells()[cellIndex]->setBlock(std::make_shared<Undefined>());
                    }
                }
            }
            cell_to_check=-1;
        }
    }
};
