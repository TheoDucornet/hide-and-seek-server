#include "player/seeker/exp.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new ExpSeeker:: ExpSeeker object
 * 
 * 
 * @param name 
 */
ExpSeeker::ExpSeeker(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
ExpSeeker::~ExpSeeker() {}

int ExpSeeker::calc_distance(int x1, int x2) const
{
    int diff = x2 - x1 ;
    if(diff < 0) { diff = -diff ;}

    return diff ;
}

/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpSeeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    std::vector<std::shared_ptr<MazeCell>> cells = observation->getCells() ;
    std::tuple<int,int> obs_size = observation->getSize() ;
    uint emplacement = (getLocation().y * std::get<0>(obs_size)) + getLocation().x ;

    for(std::shared_ptr<MazeCell> cell : cells)
    {
        if(cell->hasPlayer() && actions.size() > 0)
        {
            std::shared_ptr<Player> player = cell->getPlayer() ;
            if(player->getRole() == Role::HIDER)
            {
                uint emplacementHider = (player->getLocation().y * std::get<0>(obs_size)) + player->getLocation().x ;
                
                int dist = calc_distance(emplacement, emplacementHider) ;
                size_t action ;
                bool isAction = false ;
                for(size_t n=0 ; n <= actions.size() ; n++)
                {
                    Orientation ori = actions[n].orientation ;
                    Interaction inte = actions[n].interaction ;

                    if(ori == TOP && inte == MOVE ){
                        if( dist > calc_distance( (emplacement - std::get<0>(obs_size)), emplacementHider) ){
                            dist = calc_distance( (emplacement - std::get<0>(obs_size)), emplacementHider) ;
                            action = n ;
                            isAction = true ;
                        }}

                    else if(ori == BOTTOM && inte == MOVE){
                        if( dist > calc_distance( (emplacement + std::get<0>(obs_size)), emplacementHider) ){
                            dist = calc_distance( (emplacement + std::get<0>(obs_size)), emplacementHider) ;
                            action = n ;
                            isAction = true ;
                        }}

                    else if(ori == LEFT && inte == MOVE ){
                        if( dist > calc_distance( (emplacement - 1), emplacementHider) ){
                            dist = calc_distance( (emplacement - 1), emplacementHider) ;
                            action = n ;
                            isAction = true ;
                        }}

                    else if(ori == RIGHT && inte == MOVE ){
                        //std::cout << dist << " < " << calc_distance( (emplacement + 1 ), emplacementHider) << std::endl ;
                        if( dist > calc_distance( (emplacement + 1 ), emplacementHider) ){
                            dist = calc_distance( (emplacement + 1 ), emplacementHider) ;
                            action = n ;
                            isAction = true ;
                        }}
                }
                if(isAction)
                    return actions[action] ;
            }
        }
    }
    
    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    
    return actions[rGenerator->getRandomInt(actions.size() - 1)]; 
}

void ExpSeeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
    //std::cout << interactionToString(action.interaction) << " : " << orientationToString(action.orientation) << std::endl ;
};
