#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_EXP2_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_EXP2_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief expert Seeker class
 * 
 */
class ExpSeeker2 : public Seeker
{
public:
    ExpSeeker2(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    
    ~ExpSeeker2();

    int calc_distance(int, int, int, int) const ;
    const PlayerAction &GoTo(const std::vector<PlayerAction> &actions, int, int) const ;
    const PlayerAction &GoFarFromSeeker(const std::vector<PlayerAction> &actions) const ;
    const PlayerAction &Random(const std::vector<PlayerAction> &actions) const ;

private:
    Point position ;
    Point position_Hider ; 
    Point position_Seeker ;

    int searchX ;
    int searchY ;
    
    bool HiderSeen = false;
    bool SeekerSeen = false ;
    bool Searching = false ;

    int distance_seeker = 0;
};

#endif