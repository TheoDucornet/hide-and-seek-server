#include "player/seeker/exp2.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new ExpSeeker:: ExpSeeker object
 * 
 * 
 * @param name 
 */
ExpSeeker2::ExpSeeker2(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
ExpSeeker2::~ExpSeeker2() {}

int ExpSeeker2::calc_distance(int x1, int x2, int y1, int y2) const
{
    int diffx = x2 - x1 ;
    if(diffx < 0) { diffx = -diffx ;}
    int diffy = y2 - y1 ;
    if(diffy < 0) { diffy = -diffy ;}

    return diffx + diffy ;
}

const PlayerAction &ExpSeeker2::Random(const std::vector<PlayerAction> &actions) const
{
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    return actions[rGenerator->getRandomInt(actions.size() - 1)] ;
}

const PlayerAction &ExpSeeker2::GoTo(const std::vector<PlayerAction> &actions, int px, int py) const
{       
    int dist = calc_distance(px, position.x, py, position.y) ;
    size_t action ;
    bool isAction = false ;
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        Orientation ori = actions[n].orientation ;
        Interaction inte = actions[n].interaction ;

        if(ori == TOP && inte == MOVE ){
            if( dist > calc_distance( px, position.x, py, position.y-1 )){
                dist = calc_distance( px, position.x, py, position.y-1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == BOTTOM && inte == MOVE){
            if( dist > calc_distance( px, position.x, py, position.y+1) ){
                dist = calc_distance( px, position.x, py, position.y+1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == LEFT && inte == MOVE ){
            if( dist > calc_distance( px, position.x-1, py, position.y )){
                dist = calc_distance( px, position.x-1, py, position.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == RIGHT && inte == MOVE ){
            if( dist > calc_distance( px, position.x+1, py, position.y) ){
                dist = calc_distance( px, position.x+1, py, position.y) ;
                action = n ;
                isAction = true ;
            }}
    }
    if(isAction)
        return actions[action] ;
    else{
        return Random(actions) ;
    }
}

const PlayerAction &ExpSeeker2::GoFarFromSeeker(const std::vector<PlayerAction> &actions) const 
{
    int Hx = position_Seeker.x ;
    int Hy = position_Seeker.y ;
    
    int dist = calc_distance(Hx, position.x, Hy, position.y) ;
    size_t action ;
    bool isAction = false ;
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        Orientation ori = actions[n].orientation ;
        Interaction inte = actions[n].interaction ;
        if(ori == TOP && inte == MOVE ){
            if( dist > calc_distance( Hx, position.x, Hy, position.y-1 )){
                dist = calc_distance( Hx, position.x, Hy, position.y-1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == BOTTOM && inte == MOVE){
            if( dist > calc_distance( Hx, position.x, Hy, position.y+1) ){
                dist = calc_distance( Hx, position.x, Hy, position.y+1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == LEFT && inte == MOVE ){
            if( dist > calc_distance( Hx, position.x-1, Hy, position.y )){
                dist = calc_distance( Hx, position.x-1, Hy, position.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == RIGHT && inte == MOVE ){
            if( dist > calc_distance( Hx, position.x+1, Hy, position.y) ){
                dist = calc_distance( Hx, position.x+1, Hy, position.y) ;
                action = n ;
                isAction = true ;
            }}
    }
    if(isAction){
        return actions[action] ;
    }
    else{
        return Random(actions) ;
    }
}

/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpSeeker2::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

        // go closer to hider
    if(HiderSeen){
        return GoTo(actions, position_Hider.x, position_Hider.y) ;
    }
        // go far from the other seeker
    /*else if(SeekerSeen){
        return GoFarFromSeeker(actions) ;
    }*/
        // random action
    else{
        return Random(actions) ;
    }
}

void ExpSeeker2::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    position = getLocation() ;
    std::vector<std::shared_ptr<MazeCell>> cells = next->getCells() ;

    if(distance_seeker > 25)
        SeekerSeen = false ;

    if(position == position_Hider)
        HiderSeen = false ;

    for(std::shared_ptr<MazeCell> cell : cells)
        if(cell->hasPlayer()){
            if(cell->getPlayer()->getRole() == Role::HIDER)
            {
                position_Hider = cell->getPlayer()->getLocation() ;
                HiderSeen = true ;
            }
            else if(cell->getPlayer()->getRole() == Role::SEEKER)
            {
                position_Seeker = cell->getPlayer()->getLocation() ;
                SeekerSeen = true ;
                distance_seeker = calc_distance(position.x, position_Seeker.x, position.y, position_Seeker.y) ;
            }
        }
};
