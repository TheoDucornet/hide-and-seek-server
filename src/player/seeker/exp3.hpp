#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_EXP3_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_EXP3_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief Expert Seeker class who is able to memorize a maze based on observations.
 * 
 * Instead of random movement, if there is no hider in sight, this seeker try to explore unknown cells
 * 
 */
class ExpSeeker3 : public Seeker
{
public:
    ExpSeeker3(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    
    ~ExpSeeker3();

    int calc_distance(int, int, int, int) const ;

private:
    Point position ;
    Point position_Hider ; 
    bool HiderSeen = false;

    std::shared_ptr<Maze> maze;
    int cell_to_check = -1;
};

#endif