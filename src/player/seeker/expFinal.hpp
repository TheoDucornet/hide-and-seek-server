#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_EXPFINAL_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_EXPFINAL_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief expert Seeker class
 * 
 */
class ExpSeekerFinal : public Seeker
{
public:
    ExpSeekerFinal(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    
    ~ExpSeekerFinal();

private:
    Point position ;
    Point position_Hider ; 
    Point position_Seeker ;

    int searchX ;
    int searchY ;
    
    bool HiderSeen = false;
    bool SeekerSeen = false ;
    bool Searching = false ;

    int calc_distance(int, int, int, int) const ;
    const PlayerAction &GoTo(const std::vector<PlayerAction> &actions, int, int) const ;
    const PlayerAction &Random(const std::vector<PlayerAction> &actions) const ;


    int distance_seeker = 0;

    std::shared_ptr<Maze> maze;
    int cell_to_check = -1;

    const PlayerAction &GetDirection(const std::vector<PlayerAction> &actions) const ;
};

#endif