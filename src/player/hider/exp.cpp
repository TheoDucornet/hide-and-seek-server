#include "player/hider/exp.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new ExpHider:: ExpHider object
 * 
 * 
 * @param name 
 */
ExpHider::ExpHider(const std::string &name) : Hider(name){}


/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
ExpHider::~ExpHider() {}

int ExpHider::calc_distance(int x1, int x2, int y1, int y2) const
{
    int diffx = x2 - x1 ;
    if(diffx < 0) { diffx = -diffx ;}
    int diffy = y2 - y1 ;
    if(diffy < 0) { diffy = -diffy ;}

    return diffx + diffy ;
}

const PlayerAction &ExpHider::Random(const std::vector<PlayerAction> &actions) const
{
    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    return actions[rGenerator->getRandomInt(actions.size() - 1)]; 
}

const PlayerAction &ExpHider::GoFar(const std::vector<PlayerAction> &actions) const
{           
    int dist = calc_distance(position.x, position_seeker.x, position.y, position_seeker.y) ;
    bool isAction = false ;
    size_t action ;
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        Orientation ori = actions[n].orientation ;
        Interaction inte = actions[n].interaction ;
        
        const Orientation oriSeeker = GoTo(actions, position_seeker.x, position_seeker.y).orientation ;
    
        if(inte == PLACE && ori == oriSeeker){
            //std::cout << orientationToString(oriSeeker) << std::endl ;
            return actions[n] ;
        }
        else if(ori == TOP && inte == MOVE && position.y > 0){
            if( dist < calc_distance( position.x, position_seeker.x, position.y-1, position_seeker.y) ){
                dist = calc_distance( position.x, position_seeker.x, position.y-1, position_seeker.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == BOTTOM && inte == MOVE){
            if( dist < calc_distance( position.x, position_seeker.x, position.y+1, position_seeker.y) ){
                dist = calc_distance( position.x, position_seeker.x, position.y+1, position_seeker.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == LEFT && inte == MOVE ){
            if( dist < calc_distance( position.x-1, position_seeker.x, position.y, position_seeker.y ) ){
                dist = calc_distance( position.x-1, position_seeker.x, position.y, position_seeker.y) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == RIGHT && inte == MOVE ){
            if( dist < calc_distance( position.x+1, position_seeker.x, position.y, position_seeker.y ) ){
                dist = calc_distance( position.x+1, position_seeker.x, position.y, position_seeker.y ) ;
                action = n ;
                isAction = true ;
            }}
        else if(!isAction && inte == CARRY){
            action = n ;
            isAction = true ;
        }     
    }
        if(isAction){
            return actions[action] ;
        }
        else{
            return Random(actions) ;
        }
}

const PlayerAction &ExpHider::GoTo(const std::vector<PlayerAction> &actions, int px, int py) const
{
    int dist = calc_distance(position.x, px, position.y, py) ;
    size_t action ;
    bool isAction = false ;
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        Orientation ori = actions[n].orientation ;
        Interaction inte = actions[n].interaction ;

        /*if(inte == CARRY){
            return actions[n] ;
        }*/
        if(ori == TOP && inte == MOVE && position.y > 0){
            if( dist > calc_distance( position.x, px, position.y, py-1 )){
                dist = calc_distance( position.x, px, position.y, py-1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == BOTTOM && inte == MOVE){
            if( dist > calc_distance( position.x, px, position.y, py+1) ){
                dist = calc_distance( position.x, px, position.y, py+1) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == LEFT && inte == MOVE ){
            if( dist > calc_distance( position.x, px-1, position.y, py )){
                dist = calc_distance( position.x, px-1, position.y, py) ;
                action = n ;
                isAction = true ;
            }}
        else if(ori == RIGHT && inte == MOVE ){
            if( dist > calc_distance( position.x, px+1, position.y, py) ){
                dist = calc_distance( position.x, px+1, position.y, py) ;
                action = n ;
                isAction = true ;
            }}
    }
    if(isAction){
        return actions[action] ;
    }
    else{
        return Random(actions) ;
    }
}

const PlayerAction &ExpHider::awareness(const std::vector<PlayerAction> &actions) const
{
    for(size_t n=0 ; n <= actions.size() ; n++)
    {
        if(actions[n].interaction == NOTHING && actions[n].orientation == vision)
            return actions[n] ;
    }
    return Random(actions) ;
}


/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpHider::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    if(seekerSeen){
        return GoFar(actions) ;
    }
    else{
        return awareness(actions) ;
    }        
}

void ExpHider::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    position = getLocation() ;
    
    if(seekerSeen){
        distance_seeker = calc_distance(position.x, position_seeker.x, position.y, position_seeker.y) ;
        if(distance_seeker > 10)
            seekerSeen = false ;
    }

    std::vector<std::shared_ptr<MazeCell>> cells = next->getCells() ;
    
    //std::reverse(cells.begin(), cells.end()) ;

    for(std::shared_ptr<MazeCell> cell : cells){

        if(cell->hasPlayer()){
            if(cell->getPlayer()->getRole() == Role::SEEKER){
                position_seeker = cell->getPlayer()->getLocation() ;
                distance_seeker = calc_distance(position.x, position_seeker.x, position.y, position_seeker.y) ;
                //std::cout << distance_seeker << std::endl ;
                seekerSeen = true ;
            }
        }
    }

    vision = action.orientation ;
    
    switch(vision)
    {
        case TOP:   vision = BOTTOM ;   break ;
        case BOTTOM:   vision = TOP ;   break ;
        case LEFT:   vision = RIGHT ;   break ;
        case RIGHT:   vision = LEFT ;   break ;
        default:    vision = action.orientation ; break ;
    }
}