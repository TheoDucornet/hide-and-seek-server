#ifndef HIDE_AND_SEEK_HIDER_PLAYER_EXP_HPP
#define HIDE_AND_SEEK_HIDER_PLAYER_EXP_HPP

#include <string>
#include <memory>

#include "player/hider/hider.hpp"
#include "maze/maze.hpp"

/**
 * @brief expert hider class
 * 
 */
class ExpHider : public Hider
{
public:
    ExpHider(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    
    ~ExpHider();

    int calc_distance(int, int, int, int) const ;
    const PlayerAction &GoTo(const std::vector<PlayerAction> &actions, int px, int py) const ;
    const PlayerAction &GoFar(const std::vector<PlayerAction> &actions) const ;
    const PlayerAction &Random(const std::vector<PlayerAction> &actions) const ;
    const PlayerAction &awareness(const std::vector<PlayerAction> &actions) const ;

private:
    Point position ;
    Point position_seeker ;

    bool seekerSeen = false ;
    bool blockFound = false ;

    int distance_seeker ;
    int blockX ;
    int blockY ;

    Orientation vision ;
};

#endif