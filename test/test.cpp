#define CATCH_CONFIG_MAIN

#include "catch.hpp"

// game includes
#include "game.hpp"
#include "utils/helper.hpp"

// maze includes
#include "factory/mazeFactory.hpp"
#include "maze/custom/basic.hpp"
#include "maze/custom/default.hpp"
#include "maze/custom/closed.hpp"
#include "maze/custom/open.hpp"

// player includes
#include "factory/playerFactory.hpp"
#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"
#include "player/seeker/expFinal.hpp"
#include "player/hider/exp.hpp"

namespace Params {

    std::string hiderClassName = "random";
    std::string seekerClassName = "random";
    std::string mazeClassName = "basic"; // by default

    unsigned int nGames = 50;
};

// do not change this test!!!
TEST_CASE("check static params", "Game Params")
{
    REQUIRE(GameParams::height == 20);
    REQUIRE(GameParams::width == 20);
    REQUIRE(GameParams::nRounds == 200);
    REQUIRE(GameParams::nSeekers == 2);
    REQUIRE(GameParams::nHiders == 2);
    REQUIRE(GameParams::pctBlock == 0.40);
    REQUIRE(GameParams::pctWood == 0.08);
    REQUIRE(GameParams::pctStone == 0.04);
    REQUIRE(GameParams::pctMetal == 0.02);
}

TEST_CASE("check maze basic", "basic")
{
    // TODO: check your own maze
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<BasicMaze>("basic");

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {
        
        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
        
        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    //show state of the map
    std::cout<<"Labyrinthe Basic : "<<std::endl;
    std::cout<<maze->toString()<<std::endl;


    // check maze of current created game 
    REQUIRE(Helper::isValid(game));
  }

TEST_CASE("check maze default", "default")
{
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<DefaultMaze>("default");

    auto maze = MazeFactory::create("default", GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {
        
        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
        
        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    //show state of the map
    std::cout<<"Labyrinthe Default : "<<std::endl;
    std::cout<<maze->toString()<<std::endl;


    // check maze of current created game 
    REQUIRE(Helper::isValid(game));
  }

TEST_CASE("check maze closed", "closed")
{
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<ClosedMaze>("closed");

    auto maze = MazeFactory::create("closed", GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {
        
        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
        
        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    //show state of the map
    std::cout<<"Labyrinthe Closed : "<<std::endl;
    std::cout<<maze->toString()<<std::endl;


    // check maze of current created game 
    REQUIRE(Helper::isValid(game));
  }

TEST_CASE("check maze open", "open")
{
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<OpenMaze>("open");

    auto maze = MazeFactory::create("open", GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {
        
        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
        
        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    //show state of the map
    std::cout<<"Labyrinthe Open: "<<std::endl;
    std::cout<<maze->toString()<<std::endl;


    // check maze of current created game 
    REQUIRE(Helper::isValid(game));
}

//95 percent of victory vs random with final seeker
  TEST_CASE("check seeker victory", "expFinal vs random")
  {
    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<ExpSeekerFinal>("random");
    MazeFactory::registerClass<DefaultMaze>("basic");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    std::cout<< "Lancement de "<<Params::nGames<<" simulations"<<std::endl;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {
            
            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
            
            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        // before running
        REQUIRE(Helper::isValid(game));
        if (!Helper::isValid(game)) {
            std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        }

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();
            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // update the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();

        // update stat
        results[winner] += 1;
    }

    std::cout<<"Percent of victory for seekers :"<<results[Role::SEEKER] / (double)Params::nGames*100<<" %"<<std::endl;

    // at least 95 percent of victory for seekers
    REQUIRE(results[Role::SEEKER] / (double)Params::nGames > 0.95);
}

//95 percent of victory vs random with final hider
  TEST_CASE("check hider victory", "random vs exp")
  {
    // Need to register expected class
    HiderFactory::registerClass<ExpHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<ClosedMaze>("basic");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    std::cout<< "Lancement de "<<Params::nGames<<" simulations"<<std::endl;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {
            
            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
            
            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        // before running
        REQUIRE(Helper::isValid(game));
        if (!Helper::isValid(game)) {
            std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        }

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();
            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // update the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();

        // update stat
        results[winner] += 1;
    }

    std::cout<<"Percent of victory for hiders :"<<results[Role::HIDER] / (double)Params::nGames*100<<" %"<<std::endl;

    // at least 95 percent of victory for seekers
    REQUIRE(results[Role::HIDER] / (double)Params::nGames > 0.95);
}